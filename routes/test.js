var express = require('express');
var router = express.Router();

router.get('/test', function(req, res) {
  res.send('POST request to the homepage')
});

/**
 * 200 (OK) with json response example.
 */
router.post('/okjson', function(req, res) {

  res.header("Content-Type",'application/json');
  res.status(200);
  res.send("{\"property\": \"salami\"}")
});

/**
 * 400 (BAD REQUEST) example.
 */
router.post('/badrequest', function(req, res) {

  res.header("Content-Type",'application/json');
  res.status(400);
  res.send("{\"message\": \"something happened...\"}")
});

/**
 * 404 (NOT FOUND) example.
 */
router.post('/notfound', function(req, res) {

  res.header("Content-Type",'application/json');
  res.status(404);
  res.send("{\"message\": \"something was not found...\"}")
});

/**
 * 200 (OK) with plaintext response example.
 */
router.get('/okstring', function(req, res) {

  res.header("Content-Type",'text/plain');
  res.status(200);
  res.send("Some message...")
});

/**
 * 200 (OK) with wildcard example.
 */
router.post('/okjson/**', function(req, res) {

  res.header("Content-Type",'application/json');
  res.status(200);
  res.send("{\"property\": \"salami\"}")
});

module.exports = router;
