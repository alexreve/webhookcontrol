
# 1. Goal
This service is a simple tool allowing webhook-like functionality for testing purposes.

What it actually does is run an *express* HTTP server, exposing a number of endpoints (through routes).
Then on top of that it uses *ngrok* service, enabling monitoring and exposing the http server to the outside world. 

Endpoints can be created, updated or deleted by modifying the existing routes or adding new ones.

# 2. Requirements

In order for this service to operate the following setup is required:

* node.js (version >= 8.0.3)
* npm

# 3 Usage

## 3.1 Before starting

In order to run this service, dependencies have to be installed first.

To do that, open a terminal on project level (actually wherever package.json is)
and type: 
```bash
$ npm install
```
Depending on your node.js setup, you might have to use sudo:
```bash
$ sudo npm install
```
## 3.2 Running

### 3.2.1 Terminal

In order to run the service using a terminal, go to project level dir (actually wherever package.json is)
and type:
```bash
$ npm start
```

### 3.2.2 IntelliJ Idea

The service may also be run through the IntelliJ Idea IDE just by right-clicking the *bin/www* file
and then hitting run.

This however, might require some extra configuration steps beforehand.
(Add node.js plugin, set JavaScript version to ECMAScript 6 or more.)

## 4. Result

Once the service starts and connects to ngrok service, the publicly-accessible URL will be displayed in the console.
That URL may be used by the outside world to use the HTTP service.

Monitoring (ngrok UI) can be found at http://localhost:4040

## 5. Configuration

If by any chance the HTTP server port needs to be changed (default is 3000),
it may be done either by setting it in an environment variable (process.env.PORT) or by hand in the *bin/www* file.
